# -*- coding: utf-8 -*-

import string

VIDEO_PREFIX = '/video/svtplay'
TITLE = 'SVT Play'
SVT_URL = 'http://www.svtplay.se'


def Start():
    Plugin.AddViewGroup("List", viewMode="List", mediaType="items")
    ObjectContainer.title1 = TITLE


def DirectoryMenu(collection):
    oc = ObjectContainer()
    for c in collection:
        oc.add(DirectoryObject(
            key = Callback(DirectoryMenu, uri=c['uri']),
            title = c['title']
        ))
    return oc


@handler(VIDEO_PREFIX, TITLE)
def MainMenu():
    oc = ObjectContainer(
        objects = [
            DirectoryObject(
                key = Callback(CategoryMenu),
                title = "Kategorier"
            ),
            DirectoryObject(
                key = Callback(AlphaMenu),
                title = u"Program A-Ö"
            ),
        ]
    )
    return oc


@route(VIDEO_PREFIX + '/category')
def CategoryMenu():
    oc = ObjectContainer()
    collection = LoadCategories()
    for c in collection:
        oc.add(DirectoryObject(
            key = Callback(SubCategoryMenu, uri=c['uri']),
            title = c['title']
        ))
    return oc


@route(VIDEO_PREFIX + '/subcategory')
def SubCategoryMenu(uri):
    oc = ObjectContainer()
    collection = LoadPrograms(uri, 'titles')
    for c in collection:
        oc.add(DirectoryObject(
            key = Callback(ProgramMenu, uri=c['uri']),
            title = c['title']
        ))
    return oc


@route(VIDEO_PREFIX + '/program')
def ProgramMenu(uri):
    oc = ObjectContainer()
    collection = LoadPrograms(uri, 'episodes')
    for c in collection:
        uri = SVT_URL + c['uri']
        # Following should be moved to a callback function when the
        # framework supports it.
        json_data = JSON.ObjectFromURL(uri + '?output=json')
        try:
            json_uri = json_data['video']['videoReferences'][1]['url']
            uri = HTTPLiveStreamURL(json_uri)
        except:
            json_uri = json_data['video']['videoReferences'][0]['url']
            uri = RTMPVideoURL(json_uri)
        # mo = MediaObject(parts=[PartObject(key=Callback(Episode, uri=uri))])
        mo = MediaObject(parts=[PartObject(key=uri)])
        vco = VideoClipObject(title=c['title'], url=uri)
        vco.add(mo)
        oc.add(vco)
    return oc


@route(VIDEO_PREFIX + '/episode')
def Episode(uri):
    json_data = JSON.ObjectFromURL(uri + '?output=json')
    try:
        json_uri = json_data['video']['videoReferences'][1]['url']
        uri = HTTPLiveStreamURL(json_uri)
    except:
        json_uri = json_data['video']['videoReferences'][0]['url']
        uri = RTMPVideoURL(json_uri)
    return uri


@route(VIDEO_PREFIX + '/alpha')
def AlphaMenu():
    oc = ObjectContainer()
    return oc


def LoadCategories():
    categories = []
    categoryTags = HTML.ElementFromURL(SVT_URL + '/kategorier').xpath('//a[contains(@class, "playCategoryLink")]')
    for cat in categoryTags:
        title = cat.xpath('.//h2')[0].text
        href = cat.get('href')
        uri = href if href[0] == '/' else '/' + href
        categories.append({'title': title, 'uri': uri})
    return categories


def LoadPrograms(uri, kind):
    programs = []
    programTags = HTML.ElementFromURL(SVT_URL + uri + '/?tab=%s&sida=100' % kind).xpath('//div[contains(@class, "playBoxBody")]//a[contains(@class, "playLink")]')
    for prog in programTags:
        title = string.strip(prog.xpath('.//h5')[0].text)
        href = prog.get('href')
        uri = href if href[0] == '/' else '/' + href
        programs.append({'title': title, 'uri': uri})
    return programs
